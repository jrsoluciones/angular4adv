'use strict'

var mongoose = require('mongoose');
var app = require('./app');
var port = process.env.PORT || 3789;

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/zoo',{ useMongoClient: true })
    .then(() => {
        console.log('La conexion a la base de datos zoo se ha realizado correctamente');
        app.listen(port,()=>{
            console.log(`corriendo por url http://127.0.0.1:${port}`);
        });
    })
    .catch(err => {
        console.log(err);
    });