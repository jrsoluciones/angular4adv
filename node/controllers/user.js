'use strict'
//modulos
var bcrypt = require('bcrypt-nodejs');

//modelos
var User = require('../models/user');

//servicio jwt
var jwt = require('../services/jwt');

function pruebas(req,res){
    res.status(200).send({
        message: 'Probando'
    });
}

function saveUser(req,res){
    //crear el objeto del usuario
    var user = new User();
    var params = req.body;
    console.log(params);
    if(params.password && params.name && params.surname && params.email){
        //asigno
        user.name = params.name;
        user.surname = params.surname;
        user.email = params.email;        
        user.role = 'ROLE_USER';
        user.image = null;

        User.findOne({email: user.email.toLowerCase()}, (err,issetuser) =>{
            if(err){
                res.status(500).send({message: 'Error al comprobar el usuario'});
            }else{
                if(!issetuser){
                    //cifro la contraseña
                    bcrypt.hash(params.password,null,null,(err,hash)=>{
                        user.password = hash;
                    });        

                    //guardo usuario
                    user.save((err,userStored)=>{
                        if(err){
                            res.status(500).send({message: 'Error al guardar el usuario'});
                        }else{
                            if(!userStored){
                                res.status(400).send({message: 'No se ha registrado el usuario'}); 
                            }else{
                                res.status(200).send({user: userStored}); 
                            }
                        }
                    });
                }else{
                    res.status(200).send({
                        message: 'El usuario no puede registrarse, ya existe'
                    });
                }
            }
        });            
    }
    else
    {
        res.status(200).send({
            message: 'Introduce los datos correctamente para poder registrar al usuario'
        });
    }    
}

function login(req,res){
    var params = req.body;

    var email = params.email;
    var password = params.password;

     User.findOne({email: email.toLowerCase()}, (err,user) =>{
        if(err){
            res.status(500).send({message: 'Error al comprobar el usuario'});
        }else{
            if(user){

                bcrypt.compare(password,user.password, (err,check)=>{
                    if(check){
                        if(params.gettoken){
                            res.status(200).send({
                                token: jwt.createToken(user)
                            });
                        }
                        else
                        {
                            res.status(200).send({
                                user
                            });
                        }
                        
                    }else{
                        res.status(404).send({
                            message: 'Password incorrecta'
                        });
                    }
                });
            }else{
                res.status(404).send({
                    message: 'El usuario no ha podido logearse'
                });
            }
        }
    });       
}

module.exports = {
    pruebas,
    saveUser,
    login
}