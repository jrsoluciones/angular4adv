import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common'

/* para que funcione el Two-Way-Data binding */
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

//rutas
import { AdminRoutingModule } from "./admin.routing.module";

import { MainComponent } from "./components/main/main.component";
import { AddComponent } from "./components/add/add.component";
import { ListComponent } from "./components/list/list.component";
import { EditComponent } from "./components/edit/edit.component";

@NgModule({
    declarations:[
        MainComponent,
        AddComponent,
        ListComponent,
        EditComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        HttpModule,
        AdminRoutingModule
    ],
    exports:[
        MainComponent,
        AddComponent,
        ListComponent,
        EditComponent
    ],
    providers: []
})
export class AdminModule { }

