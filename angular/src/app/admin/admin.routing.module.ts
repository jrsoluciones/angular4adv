import { ModuleWithProviders } from '@angular/core';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MainComponent } from "./components/main/main.component";
import { AddComponent } from "./components/add/add.component";
import { ListComponent } from "./components/list/list.component";
import { EditComponent } from "./components/edit/edit.component";

const adminRoutes: Routes = [
    //{path: '', component: HomeComponent},
    {
        path: 'admin-panel', 
        component: MainComponent,
        children: [
            {path: '', redirectTo: 'listado', pathMatch: 'full' },
            {path: 'listado', component: ListComponent},
            {path: 'crear', component: AddComponent},
            {path: 'editar', component: EditComponent}
        ]
    }    
];

@NgModule({
  imports: [
    RouterModule.forChild(adminRoutes)
  ],
  exports:[
      RouterModule
  ]
})
export class AdminRoutingModule { }

