import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
/* para que funcione el Two-Way-Data binding */
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
/* Importar routing */
import { routing, appRoutingProviders } from './app.routing';

/* importar modulos */
import { ModuloEmailModule } from "./moduloemail/moduloemail.module";
import { AdminModule } from "./admin/admin.module";

/* importar componentes */
import { AppComponent } from './app.component';
import { TiendaComponent } from './components/tienda/tienda.component';
import { ParquesComponent } from './components/parques/parques.component';
import { AnimalsComponent } from './components/animals/animals.component';
import { ContactComponent } from './components/contact/contact.component';
import { HomeComponent } from './components/home/home.component';
import { KeeperComponent } from './components/keeper/keeper.component';
import { SimpleTinyComponent } from './components/simple-tiny/simple-tiny.component';



@NgModule({
  declarations: [
    AppComponent,
    TiendaComponent,
    ParquesComponent,
    AnimalsComponent,
    ContactComponent,
    HomeComponent,
    KeeperComponent,
    SimpleTinyComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    BrowserAnimationsModule,
    ModuloEmailModule,
    routing,
    AdminModule
  ],
  providers: [appRoutingProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
