import { Component, OnInit, DoCheck } from '@angular/core';

@Component({
  selector: 'guardar-email',
  template: `
    <h4>{{title}}</h4>
    <input type="text" [(ngModel)]="emailContacto" />
    <button (click)="guardarEmail()">Guardar Email</button>
  `
})
export class GuardarEmailComponent implements OnInit,DoCheck {
    

    title = 'Guardar Email';
    emailContacto: string;

    ngOnInit(): void {
        //throw new Error("Method not implemented.");
    }

    ngDoCheck(): void {
        //throw new Error("Method not implemented.");
    }

    guardarEmail(){
        localStorage.setItem('emailContacto',this.emailContacto);    
    } 
}
