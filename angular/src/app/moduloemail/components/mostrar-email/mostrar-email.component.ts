import { Component, OnInit, DoCheck } from '@angular/core';

@Component({
  selector: 'mostrar-email',
  template: `    
    <span *ngIf="emailContacto">
        <h4>{{title}}</h4>
        Usuario: {{emailContacto}}
        <button (click)="borrarEmail()">Eliminar email de contacto</button>
    </span>
  `
})
export class MostrarEmailComponent implements OnInit,DoCheck {
  title = 'Mostrar Email';
  emailContacto: string;

  ngOnInit(){
    this.emailContacto = localStorage.getItem('emailContacto');
    //console.log(localStorage.getItem('emailContacto'));
  }

  ngDoCheck(){
    this.emailContacto = localStorage.getItem('emailContacto');
  }

  borrarEmail(){
    localStorage.removeItem('emailContacto');
    localStorage.clear();
    this.emailContacto = null;
    //localStorage.clear();
  }
}
