import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common'

/* para que funcione el Two-Way-Data binding */
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

//importar componentes
import { MainEmailComponent } from './components/main-email/main-email.component';
import { GuardarEmailComponent } from './components/guardar-email/guardar-email.component';
import { MostrarEmailComponent } from "./components/mostrar-email/mostrar-email.component";

@NgModule({  
  imports: [
    FormsModule,
    CommonModule
  ],
  declarations: [
    MainEmailComponent,
    GuardarEmailComponent,
    MostrarEmailComponent
  ],
  exports: [MainEmailComponent]
})
export class ModuloEmailModule { }
