import { Component, OnInit } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { fadeIn } from "../animation";

@Component({
  selector: 'tienda',
  templateUrl: './tienda.component.html',
  styleUrls: ['./tienda.component.css'],
  animations:[
    trigger('marcar',[
      state('inactive', style({
        border: '5px solid #ccc'
      })),
      state('active',style({
        border:'5px solid yellow',
        backgroud: 'red',
        borderRadius: '50px',
        transform: 'scale(1.2)'
      })),
      transition('inactive => active', animate('1000ms ease-in')),
      transition('active => inactive', animate('1000ms ease-out'))
    ]),
    fadeIn
  ]
})
export class TiendaComponent implements OnInit {
  public titulo:string;
  public nombreDelParque:string;
  public miParque;
  public state;
  
  constructor() { 
    this.titulo = "Esta es la tienda";
    this.nombreDelParque="";
    this.state = 'inactive';   
  }

  ngOnInit() {
    $("#botonJQ").click(()=>{
      console.log('Hola JQ');
    });

    $("#caja").dotdotdot({});
  }

  cambiarEstado(status){
    if(status=='inactive'){
      this.state = 'active';
    }else
    {
      this.state= 'inactive';
    }
  }

  mostrarNombre(){
    console.log(this.nombreDelParque);
  }

  verDatosParques(event){
    console.log(event);
    this.miParque = event;
  }

  keyupHandlerFunction(a){
    console.log(a);
  }

}
