import { Component, OnInit, 
          Input, Output, EventEmitter, 
          OnChanges, SimpleChanges, DoCheck,
          OnDestroy } from '@angular/core';

@Component({
  selector: 'parques',
  templateUrl: './parques.component.html',
  styleUrls: ['./parques.component.css']
})
export class ParquesComponent implements OnInit ,OnChanges, DoCheck,OnDestroy {
  @Input() nombre:string;
  @Input('metros_cuadrados') metros:number;
  public abierto: boolean;
  public vegetacion: string;

  @Output() pasameLosDatos = new EventEmitter();

  constructor() {
    this.nombre = 'Parque Natural para caballos';
    this.metros = 450;
    this.vegetacion = 'Alta';
    this.abierto = false;
  }

  ngOnChanges(changes: SimpleChanges){
    console.log(changes);
  }

  ngOnInit() {
    console.log('on init lanzado');
  }

  ngDoCheck(){
    console.log('El docheck se ha ejecutado');
  }

  emitirEvento(){
    this.pasameLosDatos.emit({
      'nombre':this.nombre, 
      'metros':this.metros,
      'vegetacion':this.vegetacion,
      'abierto':this.abierto, 
    });
  }

  ngOnDestroy(){
    console.log('se va a elimnar el componente');
  }

}
